module.exports = function(app){

	app.get('/', function(req,res){

		var geoip = require('geoip-lite');
		var spanish=['CO','ES','PE','EC','GT','CU','HN','SV','NI','PY','CR','PA','GQ','BO','VE'];
		var ip = req.headers['x-forwarded-for'] || 
			req.connection.remoteAddress || 
			req.socket.remoteAddress ||
			req.connection.socket.remoteAddress;

		console.log('IP ADDRESS' + ip);
		var geo;

		if(ip.indexOf('::') >-1 ){
			//:: means Localhost or same network, this is for easy debugging
			return res.redirect('/es');
		}else if(spanish.indexOf(geo)>-1){

			geo = geoip.lookup(ip).country;
			console.log('Geo country test ' + geo);	
			return res.redirect('/es');

		}else{
			geo = geoip.lookup(ip).country;
			console.log('Geo country test ' + geo);	
			return res.resdirect('/en');
		} 
	});

	app.get('/es', function (req, res) {

		req.i18n.changeLanguage('es');
		var exists = req.i18n.exists('myKey');
		console.log(exists);
		var translation = req.t('myKey');
		res.send(translation);
	});

	app.get('/en', function (req, res) {

		req.i18n.changeLanguage('en');
		var exists = req.i18n.exists('myKey');
		console.log(exists);

		if(exists===true){
			res.send(req.t('myKey'));
		}else{

			res.send('keynotfound');
		}
	});


}
