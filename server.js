var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var user = require('./routes/routes');
var i18n = require('i18next');
var middleware = require('i18next-express-middleware');
var FsBackend = require('i18next-node-fs-backend');



i18n
	.use(middleware.LanguageDetector)
	.use(FsBackend)
	.init({
		lng: 'es',
		backend: {
			loadPath: __dirname + '/locales/{{lng}}.json',
			addPath: __dirname + '/locales/{{lng}}.missing.json'
		},
	})


app=express();

app.set('port',3000);

app.use(middleware.handle(i18n));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

user(app);

app.listen(app.get('port'), function () {
	console.log('Listening on port ' + app.get('port'));
})
